��    H      \  a   �            !     1     >  y   E     �  +   �  &   �  ,     %   L  	   r     |     �     �     �     �  	   �  F   �  N     ,   _     �     �     �     �     �     �     �  
   �     �     �     	     	  
   .	  w   9	  W   �	  	   	
     
     
  
   '
     2
     8
     F
     U
     b
     q
     x
     �
     �
  	   �
     �
  	   �
  F   �
  
                  &     =     I     X     ^  \   o     �     �     �     �  B   �  @   :  A   {  $   �  =   �           :  {  =     �     �  	   �  y   �     Y  =   m  (   �  2   �           (     >     Z     h     y     ~  
   �  K   �  O   �  0   :     k     t     �     �  
   �     �     �  
   �     �     �            
   )  m   4  W   �     �               '     =     F     V     k     x  	   �     �     �     �     �     �     �  \   
  
   g     r     {     �     �     �     �     �  |   �     b     k     ~     �  A   �  <   �  =     ;   \  =   �     �     �        =       B   2       ?   <          E      6      !            /   %      
   G       4   #   9   0   &                              (       A      -   :              7              3                                 C      8      @      $              >   D          ;           5   '      H   +          ,         1      *           F   	   "   )   .     Theme by %1$s. %1$s at %2$s (Edit) A beautifully-simple yet powerful WordPress theme that integreates perfectly with Gutenberg and the Atomic Blocks plugin. Accent Color Add a search icon to your header menu area. Adjust the size of the main body font. Adjust the size of the post and page titles. Adjust the width of the content area. All Posts All posts by %s Atomic Blocks AtomicBlocks.com Blog Body Font Size Category: Change the accent color of buttons and various typographical elements. Change the text that appears in the footer tagline at the bottom of your site. Choose the font style used across your site. Close Comment navigation Comments are closed. Content Width Disabled Docs Enabled Font Style Footer - Column 1 Footer - Column 2 Footer - Column 3 Footer Tagline Full Width It looks like nothing was found at this location. Please use the search box to locate the content you were looking for. It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Load more Menu Newer Comments Next post: Next: Nothing Found Older Comments Page Builder Page Not Found Pages: Posted by %s Posts by %s Previous post: Previous: Primary Menu Read More Ready to publish your first post? <a href="%1$s">Get started here</a>. Sans Serif Search Search Icon Search Results for: %s Search for: Search here... Serif Social Icon Menu Sorry, but nothing matched your search terms. Please try again with some different keywords. Tag: Theme Options Title Font Size Twitter Widgets added here will appear in the center column of the footer. Widgets added here will appear in the left column of the footer. Widgets added here will appear in the right column of the footer. Your comment is awaiting moderation. https://arraythemes.com/themes/atomic-blocks-wordpress-theme/ https://atomicblocks.com/ on Project-Id-Version: Atomic Blocks 1.0.8
Report-Msgid-Bugs-To: http://wordpress.org/support/theme/style
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-07-29 09:45+0200
Language-Team: 
X-Generator: Poedit 2.0.6
Last-Translator: 
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Language: cs_CZ
 Téma od %1$s. %1$s at %2$s (Upravit) Krásně jednoduché, ale výkonné téma WordPress, které se dokonale integruje s Gutenbergem a pluginem Atomic Blocks. Barva zvýraznění Přidejte ikonu vyhledávání do oblasti nabídky záhlaví. Upravte velikost písma hlavního těla. Upravte velikost názvů příspěvků a stránek. Upravte šířku oblasti obsahu. Všechny příspěvky Všechny příspěvky od %s Atomic Blocks AtomicBlocks.com Blog Velikost písma body Kategorie: Změňte barvu zvýraznění tlačítek a různých typografických prvků. Změňte text, který se zobrazí v záhlaví zápatí ve spodní části webu. Vyberte styl písma používaný na vašem webu. Zavřít Navigace v komentářích Diskuze je uzavřena. Šířka obsahu Zakázáno Docs Povoleno Styl fontu Zápatí - sloupec 1 Zápatí - sloupec 2 Zápatí - sloupec 3 Slogan v zápatí Full Width Vypadá to, že na tomto místě nebylo nic nalezeno. Hledaný obsah vyhledejte pomocí vyhledávacího pole. It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Načíst další Menu Novější komentáře Další příspěvek: Další: Není dostupná Starší komentáře Page Builder Stránka nenalezena Stránky: Zveřejněno uživatelem %s Příspěvky od %s Předchozí příspěvek: Předchozí: Hlavní nabídka Číst více Jste připraveni publikovat svůj první příspěvek? <a href="%1$s"> Začínáme zde </a>. Sans Serif Vyhledat Ikona vyhledávání Výsledky hledání pro: %s Hledat: Vyhledat zde... Serif Nabídka sociálních ikon Je nám líto, ale hledaným výrazům neodpovídá nic. Zkuste to prosím znovu s některými různými klíčovými slovy. Štítek Možnosti šablony Velikost písma (Nadpis) Twitter Widgety přidané zde se zobrazí ve středním sloupci zápatí. Widgety přidané zde se zobrazí v levém sloupci zápatí. Widgety přidané zde se zobrazí v pravém sloupci zápatí. Váš komentář čeká na posouzení moderátorem diskuse. https://arraythemes.com/themes/atomic-blocks-wordpress-theme/ https://atomicblocks.com/ na 